import './index.scss';

const contextField = document.querySelector('#context');
const questionField = document.querySelector('#question');
const submitButton = document.querySelector('#submit');
const results = document.querySelector('#results');
async function query(data) {
  const response = await fetch(
    "https://api-inference.huggingface.co/models/consciousAI/question-answering-roberta-base-s-v2",
    {
      headers: { Authorization: "Bearer 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" },
      method: "POST",
      body: JSON.stringify(data),
    }
  );
  const result = await response.json();
  return result;
}

const askQuestion = () => {
  const context = contextField.value;
  const question = questionField.value;

  const questionAndAnswer = document.createElement('div');
  const questionText = document.createElement('p');
  const answerText = document.createElement('p');

  questionText.innerHTML = question;
  questionAndAnswer.appendChild(questionText);
  questionAndAnswer.appendChild(answerText);
  results.appendChild(questionAndAnswer);

  query({ inputs: { question, context } }).then((response) => {
    console.log(JSON.stringify(response));

    answerText.innerHTML = response.answer;
  });
}

submitButton.addEventListener('click', askQuestion);

